subdown.py
=========

Downloads image links and imgur urls from subreddits.

Usage: `subdown.py <subreddit[,subreddit]> [pages]`
